const express = require('express');
const ReceiptController = require("./controllers/ReceiptController")
const UserController = require("./controllers/UserController")
const BalanceController = require("./controllers/BalanceController")
const router = express.Router();
const auth = require('./middlewares/authentification');
const multer  = require('multer')
const upload = multer({ dest: 'public/images/' });

module.exports = () => {
    let balanceController = new BalanceController();
    let receiptController = new ReceiptController();
    let userController = new UserController();

    // router.get('/plugin/:id', receiptController.getDetailsPlugin);
    // router.put('/plugin/:id', auth, upload.single('image'), receiptController.update);
    // router.delete('/plugin/:id', auth, receiptController.delete);
    // router.post('/plugin', auth, upload.single('image'), receiptController.create);
    // router.get('/plugins', receiptController.getListPluginAudio);
    // router.get('/myPlugins', auth,receiptController.getPluginOfCurrentUser);
    // router.get('/plugins/count', receiptController.count);

    //User
    router.get('/receipts',auth, receiptController.getListReceipt);
    router.get('/balances',auth, balanceController.getListBalance);
    router.post('/login', userController.login);
    router.post('/register', userController.register);
    router.get('/checkToken', userController.checkToken);
    router.get('/profile', auth, userController.profile);

    return router;
};