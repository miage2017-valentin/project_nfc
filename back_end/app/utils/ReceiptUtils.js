module.exports = class {
    computeTotalAmount(purchases){
        let totalAmount = 0;
        purchases.map(purchase => totalAmount += purchase.quantity * purchase.product.unitaryPrice);
        return totalAmount;
    }
}