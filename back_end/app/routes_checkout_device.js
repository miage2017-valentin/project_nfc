const express = require('express');
const ReceiptController = require("./controllers/ReceiptController")
const ProductController = require("./controllers/ProductController")
const UserController = require("./controllers/UserController")
const CheckoutDeviceController = require("./controllers/CheckoutDeviceController")
const router = express.Router();
const auth = require('./middlewares/authentificationCheckoutDevice');

module.exports = () => {
    let receiptController = new ReceiptController();
    let userController = new UserController();
    let checkoutDeviceController = new CheckoutDeviceController();
    let productController = new ProductController();

    router.get('/products', auth, productController.getListProduct);
    router.post('/receipt', auth, receiptController.create);

    //User
    router.post('/login', checkoutDeviceController.login);
    router.post('/register', checkoutDeviceController.register);
    router.get('/user/checkToken', userController.checkToken);
    router.get('/checkToken', checkoutDeviceController.checkToken);
    router.get('/profile', auth, checkoutDeviceController.profile);

    return router;
};