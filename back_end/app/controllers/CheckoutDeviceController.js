const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = class {

    login(req, res) {
        const login = req.body.login[0];
        const password = req.body.password[0];
        
        models.CheckoutDevice.findOne({ login: login }).exec(async (err, user) => {
            if (user !== null) {
                const valid = await bcrypt.compare(password, user.password);
                if (valid) {
                    const token = jwt.sign({ id: user._id }, process.env.SECRET_JWT, {
                        expiresIn: 86400 // expires in 24 hours
                    });
                    res.status(200).send({ auth: true, token: token });
                } else {
                    res.status(400).send({ auth: false, message: "Invalid password" });
                }
            } else {
                res.status(404).send({ auth: false, message: "User not exist" });
            }
        });
    }

    register(req, res) {
        const login = req.body.login;
        const password = req.body.password;
        const passwordConfirm = req.body.passwordConfirm;

        if(password !== passwordConfirm){
            res.status(400).send({ auth: false, message: "This password doesn't match" });
        }else{
            models.CheckoutDevice.findOne({ login: login }).exec(async (err, user) => {
                if (user === null) {
                    let checkoutDeviceObj = new models.CheckoutDevice(req.body);
                    checkoutDeviceObj.password = await bcrypt.hash(checkoutDeviceObj.password, parseInt(process.env.SALTROUNDS));
                    checkoutDeviceObj.save().then((vendor) => {
                        const token = jwt.sign({ id: vendor._id }, process.env.SECRET_JWT, {
                            expiresIn: 86400 // expires in 24 hours
                        });
                        res.status(200).send({ auth: true, token: token, message: "User created with success" });
    
                    });
                } else {
                    res.status(400).send({ auth: false, message: "Login already used" });
                }
            });
        }
    }

    checkToken(req, res){
        let token = req.query.token;
        if (token) {
            try {
                const decoded = jwt.verify(token, process.env.SECRET_JWT, { ignoreExpiration: false });
                models.CheckoutDevice.findById(decoded.id, "-password")
                .exec((err, user) => {
                    if (user !== null) {
                        req.currentUser = user;
                        return res.status(200).send({ valid: true, user });
                    } else {
                        return res.status(400).send({ valid: false });
                    }
                })
            } catch (err) {
                return res.status(400).send({ valid: false, message: "Token expired" });
            }
        } else {
            return res.status(401).send({ valid: false, token: "No token provided." });
        }
    }

    profile(req, res){
        return res.status(200).send({user: req.currentUser});
    }
}