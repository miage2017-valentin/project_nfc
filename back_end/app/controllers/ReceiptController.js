const models = require("../models");
const jwt = require('jsonwebtoken');
const EMeansOfPayment = require('../utils/EMeansOfPayment');
const ReceiptUtils = require('../utils/ReceiptUtils');
const receiptUtils = new ReceiptUtils();

module.exports = class {

    getListReceipt(req, res) {

        let condition = {};
        
        condition.client = req.currentUser;

        models.Receipt.find(condition)
            .populate('checkoutDevice', '-password -login -reference -receipts')
            .populate({path: 'purchases', populate : {path : 'product'}})
            .lean()
            .exec(async (err, receipts) => {
                for(let receipt of receipts){
                    let store = await models.Store.findById(receipt.checkoutDevice.store).exec();
                    receipt.store = store;
                    receipt.totalAmount = receiptUtils.computeTotalAmount(receipt.purchases);
                }
                return res.json(receipts);
            });
    }

    async create(req, res) {
        let receipt = req.body;
        if (receipt) {

            let objReceipt = new models.Receipt(receipt);
            objReceipt.createdAt = new Date();
            objReceipt.checkoutDevice = req.currentUser;
            objReceipt.purchases = [];

            //Get Client
            const decoded = jwt.verify(receipt.tokenClient, process.env.SECRET_JWT, { ignoreExpiration: false });
            let client = await models.User.findById(decoded.id, "-password").exec();
            objReceipt.client = client;

            // Get balance
            let balance = await models.Balance.find({
                store: req.currentUser.store,
                user: client
            }).exec();

            if(balance.length === 0){
                balance = new models.Balance({
                    amount: 0,
                    store: req.currentUser.store,
                    user: client,
                    assignLoyaltyPoints: []
                });
                await balance.save();
            }else{
                balance = balance[0];
            }


            // Check if client can pay with loyalty point
            console.log(objReceipt.meansOfPayment);
            console.log(EMeansOfPayment.PAYMENT_WITH_LOYALTY_CARD);
            if(objReceipt.meansOfPayment === EMeansOfPayment.LOYALTY_CARD){
                let totalAmount = receiptUtils.computeTotalAmount(receipt.purchases);
                if(balance.amount >= totalAmount){
                    balance.amount -= totalAmount;
                }else{
                    res.status(400).send({ message: "Insufficient background on the loyalty card" });
                    return true;
                }
            }

            console.log(balance);

            for(let purchase of receipt.purchases){
                let product = await models.Product.findById(purchase.product).exec();
                let objPurchase = new models.Purchase({
                    quantity: purchase.quantity,
                    product: product
                });

                objPurchase.assignLoyaltyPoint

                await objPurchase.save();

                if(product.loyaltyPercentage > 0){
                    let assign = new models.AssignLoyaltyPoint({
                        amount: ((product.unitaryPrice * purchase.quantity) * (product.loyaltyPercentage / 100)).toFixed(2),
                        balance: balance,
                        purchase: objPurchase
                    });

                    await assign.save();

                    balance.assignLoyaltyPoints.push(assign);
                    balance.amount += assign.amount;
                    await balance.save();
                }
                objReceipt.purchases.push(objPurchase);
            }
            
            objReceipt.save((err, object) => {
                if (err == null) {
                    res.status(201).send({ message: "Receipt created", receipt: objReceipt });
                } else {
                    res.status(400).send({ message: "Receipt not created" });
                }
            });
        } else {
            res.status(400).send({ message: "Receipt not entered" });
        }
    }
}