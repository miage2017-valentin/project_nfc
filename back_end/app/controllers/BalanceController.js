const models = require("../models");
const jwt = require('jsonwebtoken');

module.exports = class {

    getListBalance(req, res) {
        let condition = {};
        
        condition.user = req.currentUser;

        models.Balance.find(condition)
            .populate('assignLoyaltyPoints')
            .populate('store')
            .lean()
            .exec(async (err, balances) => {
                return res.json(balances);
            });
    }
}