const mongoose = require('mongoose');

let StoreSchema = new mongoose.Schema({
    name: String,
    image: String,
    address: String
});

let ProductSchema = new mongoose.Schema({
    name: String,
    reference: String,
    loyaltyPercentage: Number,
    unitaryPrice: Number,
    store: {type: mongoose.Schema.Types.ObjectId, ref:'Store'}
});

let UserSchema = new mongoose.Schema({
    email: String,
    password: String,
    name: String,
    lastname: String,
    address: String,
    city: String,
    numCardFidelity: String,
    balances: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Balance'}]
});

let PurchaseSchema = new mongoose.Schema({
    quantity: Number,
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product'}
});

let ReceiptSchema = new mongoose.Schema({
    createdAt: Date,
    meansOfPayment: String,
    purchases: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Purchase'}],
    checkoutDevice : { type: mongoose.Schema.Types.ObjectId, ref: 'CheckoutDevice'},
    client : { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
})

let CheckoutDeviceSchema = new mongoose.Schema({
    login: String,
    password: String,
    reference: String,
    store: {type: mongoose.Schema.Types.ObjectId, ref:'Store'},
    receipts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Receipt'}]
});

let BalanceSchema = new mongoose.Schema({
    amount: Number,
    store: {type: mongoose.Schema.Types.ObjectId, ref:'Store'},
    user: {type: mongoose.Schema.Types.ObjectId, ref:'User'},
    assignLoyaltyPoints: [{type: mongoose.Schema.Types.ObjectId, ref:'AssignLoyaltyPoint'}]
});

let AssignLoyaltyPointSchema = new mongoose.Schema({
    amount: Number, 
    balance: {type: mongoose.Schema.Types.ObjectId, ref:'Balance'},
    purchase: {type: mongoose.Schema.Types.ObjectId, ref:'Purchase'}
})

module.exports = {
    Store : mongoose.model('Store', StoreSchema),
    Product : mongoose.model('Product', ProductSchema),
    User: mongoose.model('User', UserSchema),
    Purchase: mongoose.model('Purchase', PurchaseSchema),
    CheckoutDevice: mongoose.model('CheckoutDevice', CheckoutDeviceSchema),
    Receipt: mongoose.model('Receipt', ReceiptSchema),
    Balance: mongoose.model('Balance', BalanceSchema),
    AssignLoyaltyPoint: mongoose.model('AssignLoyaltyPoint', AssignLoyaltyPointSchema)
};