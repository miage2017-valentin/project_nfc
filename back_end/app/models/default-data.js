const models = require('./index.js');
const bcrypt = require('bcrypt');

module.exports = {
    clear: () => {
        let listPromise = [];
        Object.keys(models).forEach((key, index) => {
            listPromise.push(models[key].deleteMany({}));
        });

        return Promise.all(listPromise);
    },
    save: async () => {

        // Create Users
        let user1 = new models.User({
            email: "francois@myFidelity.com",
            password: "test",
            name: "François",
            lastname: "LeFrançais",
            address: "Quelque part",
            city: "dans cette ville",
            numCardFidelity: "d91beaed-b6df-42ed-830f-a0fa6bc05c0e",
            balances: []
        });
        user1.password = await bcrypt.hash(user1.password, parseInt(process.env.SALTROUNDS));
        await user1.save();

        let user2 = new models.User({
            email: "roberto@myFidelity.com",
            password: "test",
            name: "Roberto",
            lastname: "LePortugais",
            address: "Quelque part",
            city: "dans cette ville",
            numCardFidelity: "8c2eb646-134f-11e9-ab14-d663bd873d93",
            balances: []
        });
        user2.password = await bcrypt.hash(user2.password, parseInt(process.env.SALTROUNDS));
        await user2.save();

        // Create Stores
        let storeAuchan = new models.Store({
            name: "Auchan Grasse",
            image: "public/images/logo_auchan.png",
            address: "200 rue de la Recherche, Grasse, France"        
        });
        await storeAuchan.save();

        let storeEclerc = new models.Store({
            name: "Eclerc Le Cannet",
            image: "public/images/logo_eclerc.png",
            address: "200 rue de la Recherche, Cannet, France"        
        });
        await storeEclerc.save();

        // Checkout device
        let checkoutDeviceAuchan = new models.CheckoutDevice({
            login: "auchanGrasse-1",
            password: "auchan",
            reference: "CKOD-1",
            store: storeAuchan
        });
        checkoutDeviceAuchan.password = await bcrypt.hash(checkoutDeviceAuchan.password, parseInt(process.env.SALTROUNDS));
        await checkoutDeviceAuchan.save();

        let checkoutDeviceEclerc = new models.CheckoutDevice({
            login: "eclercLeCannet-1",
            password: "eclerc",
            reference: "CKOD-2",
            store: storeEclerc
        });
        checkoutDeviceEclerc.password = await bcrypt.hash(checkoutDeviceEclerc.password, parseInt(process.env.SALTROUNDS));
        await checkoutDeviceEclerc.save();

        // Create Products
        let productSandwich = new models.Product({
            name: "Sandwich Sodebo",
            reference: "PT000001",
            loyaltyPercentage: 10,
            unitaryPrice: 3.69,
            store: storeAuchan
        });
        await productSandwich.save();

        let productPastaBox = new models.Product({
            name: "Pasta box carbonarra",
            reference: "PT000001",
            loyaltyPercentage: 50,
            unitaryPrice: 5,
            store: storeEclerc
        });
        await productPastaBox.save();

        let purchase1 = new models.Purchase({
            quantity: 3,
            product: productSandwich
        });
        await purchase1.save();

        let purchase2 = new models.Purchase({
            quantity: 1,
            product: productPastaBox
        });
        await purchase2.save();

        let receiptUser1 = new models.Receipt({
            createdAt: new Date(),
            meansOfPayment: "Credit Card",
            purchases: [ purchase1, purchase2],
            checkoutDevice : checkoutDeviceAuchan,
            client : user1
        });
        await receiptUser1.save();

        let purchase3 = new models.Purchase({
            quantity: 10,
            product: productSandwich
        });
        await purchase3.save();

        let purchase4 = new models.Purchase({
            quantity: 6,
            product: productPastaBox
        });
        await purchase4.save();

        let receipt2User1 = new models.Receipt({
            createdAt: new Date(),
            meansOfPayment: "Cash",
            purchases: [ purchase3, purchase4],
            checkoutDevice : checkoutDeviceAuchan,
            client : user1
        });
        await receipt2User1.save();

        let receiptUser2 = new models.Receipt({
            createdAt: new Date(),
            meansOfPayment: "Loyalty Card",
            purchases: [ new models.Purchase({
                quantity: 5,
                product: productSandwich
            }),new models.Purchase({
                quantity: 9,
                product: productPastaBox
            })],
            checkoutDevice : checkoutDeviceAuchan,
            client : user2
        });
        await receiptUser2.save();
    }
}