package fr.av.mbds.context;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import fr.av.mbds.configuration.GlobalProperties;
import lombok.Getter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Component
@Getter
@Order(2)
public class ContextCardReader {

    @Autowired
    private GlobalProperties globalProperties;

    private String token;

    @PostConstruct
    public void init(){
        try {
            this.refreshToken();
        } catch (Exception e) {
            System.out.println("Error init context: "+ e.getMessage());
        }
    }

    public void refreshToken() throws Exception {
        JSONObject body = new JSONObject();
        body.append("login", globalProperties.getLogin());
        body.append("password", globalProperties.getPassword());

        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");

        HttpResponse<JsonNode> jsonResponse = Unirest.post(globalProperties.getUrlApi()+"login")
                .headers(headers)
                .body(body)
                .asJson();

        JSONObject responseBody = jsonResponse.getBody().getObject();
        if(responseBody.getBoolean("auth")){
            this.token = responseBody.getString("token");
            System.out.println("Token: "+ this.token);
        }else{
            throw new Exception("Authentification failed !");
        }
    }
}
