package fr.av.mbds.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Getter
@PropertySource("classpath:global.properties")
@Order(1)
public class GlobalProperties {
    @Value("${url-api}")
    private String urlApi;

    @Value("${checkout-device-login}")
    private String login;

    @Value("${checkout-device-password}")
    private String password;
}
