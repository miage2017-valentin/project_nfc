package fr.av.mbds.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProductDTO {
    @JsonProperty("_id")
    private String id;
    private String name;
    private String reference;
    private Long loyaltyPercentage;
    private Long unitaryPrice;
}
