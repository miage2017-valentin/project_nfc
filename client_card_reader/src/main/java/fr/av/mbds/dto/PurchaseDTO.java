package fr.av.mbds.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class PurchaseDTO {
    @JsonProperty("_id")
    private String id;
    private int quantity;
    private ProductDTO product;
}
