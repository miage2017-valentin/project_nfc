package fr.av.mbds.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
public class ReceiptDTO {
    private Date createdAt;
    private String meansOfPayment;
    private List<PurchaseDTO> purchases;
    private String client;
    private String tokenClient;

    public String display() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT); //pretty print
        String s = om.writeValueAsString(this);
        return s;
    }
}
