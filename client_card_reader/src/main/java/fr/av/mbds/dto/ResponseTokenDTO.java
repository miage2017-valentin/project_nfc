package fr.av.mbds.dto;

import fr.av.mbds.enums.EAction;
import lombok.Getter;
import lombok.Setter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@Setter
public class ResponseTokenDTO {
    private String token;
    private EAction action;

    public ResponseTokenDTO(String token) throws Exception {
        Pattern patternAction = Pattern.compile("\\[\\|\\](.*?)\\[\\|\\]");
        Matcher matcherAction = patternAction.matcher(token);
        if (matcherAction.find()) {
            String action = matcherAction.group(1);
            this.action = EAction.valueOf(action);
        } else {
            throw new Exception("Error regex");
        }
        Pattern patternToken = Pattern.compile("^.*\\[\\|\\](.*?)$");
        Matcher matcherToken = patternToken.matcher(token);
        if (matcherToken.find()) {
            this.token = matcherToken.group(1);
        } else {
            throw new Exception("Error regex");
        }
    }
}
