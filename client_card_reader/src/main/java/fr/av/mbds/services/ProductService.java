package fr.av.mbds.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import fr.av.mbds.CardReaderApplication;
import fr.av.mbds.configuration.GlobalProperties;
import fr.av.mbds.context.ContextCardReader;
import fr.av.mbds.dto.ProductDTO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@Component
public class ProductService {

    private static Logger LOG = LoggerFactory
            .getLogger(ProductService.class);

    @Autowired
    private ContextCardReader contextCardReader;

    @Autowired
    private GlobalProperties globalProperties;

    public ArrayList<ProductDTO> getProducts() throws UnirestException {


        ArrayList<ProductDTO> products = new ArrayList<>();
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + contextCardReader.getToken());

        HttpResponse<JsonNode> jsonResponse = Unirest.get(globalProperties.getUrlApi()+"products")
                .headers(headers)
                .asJson();

        JSONArray bodyResponse = jsonResponse.getBody().getArray();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        for(int i = 0; i < bodyResponse.length(); i++){
            JSONObject json = bodyResponse.getJSONObject(i);
            ProductDTO product = null;
            try {
                product = objectMapper.readValue(json.toString(), ProductDTO.class);
                products.add(product);
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
        }

        return products;
    }
}
