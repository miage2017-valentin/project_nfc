package fr.av.mbds.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import fr.av.mbds.configuration.GlobalProperties;
import fr.av.mbds.context.ContextCardReader;
import fr.av.mbds.dto.ProductDTO;
import fr.av.mbds.dto.ReceiptDTO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@Component
public class ReceiptService {

    private ObjectMapper objectMapper;

    private static Logger LOG = LoggerFactory
            .getLogger(ReceiptService.class);

    @Autowired
    private ContextCardReader contextCardReader;

    @Autowired
    private GlobalProperties globalProperties;

    public boolean createReceipt(ReceiptDTO receiptDTO) throws UnirestException, JsonProcessingException {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-type", "application/json");
        headers.put("Authorization", "Bearer " + contextCardReader.getToken());

        LOG.info(this.getObjectMapper().writeValueAsString(receiptDTO));
        HttpResponse<JsonNode> jsonResponse = Unirest.post(globalProperties.getUrlApi()+"receipt")
                .headers(headers)
                .body(this.getObjectMapper().writeValueAsString(receiptDTO))
                .asJson();

        JSONObject bodyResponse = jsonResponse.getBody().getObject();

        //ReceiptDTO receipt = null;
        if(jsonResponse.getStatus() == 201) {
            /*try {
                receipt = this.getObjectMapper().readValue(bodyResponse.getJSONObject("receipt").toString(), ReceiptDTO.class);
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }*/
            return true;
        }
        return false;
    }

    public ObjectMapper getObjectMapper(){
        if(objectMapper == null){
            this.objectMapper = new ObjectMapper();
            this.objectMapper.setSerializationInclusion(com.fasterxml.jackson.annotation.JsonInclude.Include.ALWAYS);
            this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }

        return this.objectMapper;
    }
}
