package fr.av.mbds.services;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import fr.av.mbds.configuration.GlobalProperties;
import fr.av.mbds.context.ContextCardReader;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class UserService {

    private static Logger LOG = LoggerFactory
            .getLogger(UserService.class);

    @Autowired
    private ContextCardReader contextCardReader;

    @Autowired
    private GlobalProperties globalProperties;

    public boolean checkToken(String token){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + contextCardReader.getToken());

        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get(globalProperties.getUrlApi()+"user/checkToken")
                    .headers(headers)
                    .queryString("token", token)
                    .asJson();

            JSONObject bodyResponse = jsonResponse.getBody().getObject();
            return bodyResponse.getBoolean("valid");
        } catch (UnirestException e) {
            LOG.error(e.getMessage());
        }
        return false;
    }
}
