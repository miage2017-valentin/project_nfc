package fr.av.mbds.enums;

public enum EMeansOfPayment {
    CB("Credit Card"), CASH("Cash payment"), LoyaltyCard("Loyalty Card");

    private final String title;

    EMeansOfPayment(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
