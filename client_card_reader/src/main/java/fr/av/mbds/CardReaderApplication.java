package fr.av.mbds;

import fr.av.mbds.configuration.GlobalProperties;
import fr.av.mbds.dto.ProductDTO;
import fr.av.mbds.dto.PurchaseDTO;
import fr.av.mbds.dto.ReceiptDTO;
import fr.av.mbds.dto.ResponseTokenDTO;
import fr.av.mbds.enums.EAction;
import fr.av.mbds.enums.EMeansOfPayment;
import fr.av.mbds.services.ProductService;
import fr.av.mbds.services.ReceiptService;
import fr.av.mbds.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

import javax.smartcardio.*;

@SpringBootApplication
public class CardReaderApplication
        implements CommandLineRunner {

    @Autowired
    private GlobalProperties globalProperties;

    @Autowired
    private ProductService productService;

    @Autowired
    private ReceiptService receiptService;

    @Autowired
    private UserService userService;

    private static Logger LOG = LoggerFactory
            .getLogger(CardReaderApplication.class);

    private static Card card = null;
    private static CardChannel channel = null;
    //public static byte[] MY_SERVICE_AID = { (byte) 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, (byte) 0x12, (byte) 0x34, (byte) 0x56};
    public static byte[] myCmd = {(byte) 0xFF, (byte) 0xCA, 0x00, 0x00, 0x00};
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(CardReaderApplication.class, args);
    }

    @Override
    public void run(String... args) {
        LOG.info("URL API: " + globalProperties.getUrlApi());
        executeCommand(myCmd);
    }

    private void executeCommand(byte[] apdu) {

        boolean isValid = false;
        do {
            try {
                ArrayList<ProductDTO> products = productService.getProducts();
                LOG.info("Products found: " + products.size());

                PurchaseDTO purchaseDTO = new PurchaseDTO();
                purchaseDTO.setProduct(products.get(0));
                purchaseDTO.setQuantity(3);


                // Wait contact NFC
                ResponseTokenDTO token = this.openConnection();
                // Check token send by phone
                isValid = this.userService.checkToken(token.getToken());

                if (isValid) {
                    System.out.println("Token of user is valid");
                    // Create receipt
                    ArrayList<PurchaseDTO> purchases = new ArrayList<>();
                    purchases.add(purchaseDTO);
                    ReceiptDTO receiptDTO = new ReceiptDTO();
                    if(token.getAction() == EAction.JUST_RECEIPT){
                        System.out.println("User not pay with loyalty point");
                        receiptDTO.setMeansOfPayment(EMeansOfPayment.CB.getTitle());
                    }else{
                        System.out.println("User want pay with loyalty point");
                        receiptDTO.setMeansOfPayment(EMeansOfPayment.LoyaltyCard.getTitle());
                    }
                    receiptDTO.setPurchases(purchases);
                    receiptDTO.setTokenClient(token.getToken());


                    if (this.receiptService.createReceipt(receiptDTO)) {
                        LOG.info("Receipt created:" + receiptDTO.display());
                    }else{
                        LOG.info("Insufficient background on the loyalty card");
                        receiptDTO.setMeansOfPayment(EMeansOfPayment.CASH.toString());
                        this.receiptService.createReceipt(receiptDTO);
                    }
                } else {
                    LOG.error("Invalid token, transaction failed !");
                }


            } catch (Exception e) {
                LOG.error(e.getMessage());
            }

        } while (!isValid);
    }

    public ResponseTokenDTO openConnection() throws Exception {
        TerminalFactory factory = TerminalFactory.getDefault();
        CardTerminals cardterminals = factory.terminals();
        card = null;
        List<CardTerminal> terminals = cardterminals.list();
        System.out.println("Terminals: " + terminals);
        CardTerminal terminal = cardterminals.getTerminal(terminals.get(0).getName());
        terminal.waitForCardPresent(60000);
        System.out.println("Card detected!");
        card = terminal.connect("*");
        channel = card.getBasicChannel();

        CardChannel c = card.getBasicChannel();

        ResponseAPDU resp = c.transmit(new CommandAPDU(
                0x00, 0xA4, 0x04, 0x00, new byte[]{(byte) 0xF2, (byte) 0x22, (byte) 0x22, (byte) 0x22, (byte) 0x22}));
        assert resp.getSW() == 0x9000;
        byte[] byteArray = resp.getData();
        return new ResponseTokenDTO(new String(byteArray));
    }

}
