package fr.av.mbds.myfidelity.fragment;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import fr.av.mbds.myfidelity.ApiService;
import fr.av.mbds.myfidelity.ICallable;
import fr.av.mbds.myfidelity.R;
import fr.av.mbds.myfidelity.adapter.ReceiptAdapter;
import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.util.ItemClickSupport;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListReceiptFragment extends Fragment {
    private Context context;
    private RecyclerView listViewReceipts;
    private ICallable mCallback;
    private BroadcastReceiver broadcastReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list_receipt, container, false);
        listViewReceipts = view.findViewById(R.id.listReceipt);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        listViewReceipts.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listViewReceipts.getContext(),
                mLayoutManager.getOrientation());
        listViewReceipts.addItemDecoration(dividerItemDecoration);

        IntentFilter intFilt = new IntentFilter("REFRESH_LIST");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshListReceipt();
            }
        };
        context.registerReceiver(broadcastReceiver, intFilt);

        refreshListReceipt();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ICallable) {
            mCallback = (ICallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable");
        }
    }

    public void refreshListReceipt() {
        ApiService.getInstance(context).getReceiptList(new Callback<List<ReceiptDTO>>() {
            @Override
            public void onResponse(Call<List<ReceiptDTO>> call, Response<List<ReceiptDTO>> response) {
                List<ReceiptDTO> listReceipts = response.body();
                if(listViewReceipts.getAdapter() == null || (listReceipts != null  && listReceipts.size() != listViewReceipts.getAdapter().getItemCount())) {
                    final ReceiptAdapter adapter = new ReceiptAdapter(listReceipts, context);
                    listViewReceipts.setAdapter(adapter);
                    ItemClickSupport.addTo(listViewReceipts, R.layout.fragment_list_receipt)
                            .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                    ReceiptDTO receiptSelected = adapter.getReceipt(position);
                                    mCallback.showReceiptDetails(receiptSelected);
                                }
                            });
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                System.err.println(t.getMessage());
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            context.unregisterReceiver(broadcastReceiver);
        } catch (IllegalArgumentException e) {
            System.out.println("Receiver not registered: " + e.getMessage());
        }
    }
}
