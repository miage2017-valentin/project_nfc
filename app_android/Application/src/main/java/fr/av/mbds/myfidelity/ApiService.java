package fr.av.mbds.myfidelity;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import fr.av.mbds.myfidelity.dto.BalanceDTO;
import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.dto.RegisterDTO;
import fr.av.mbds.myfidelity.dto.RequestLoginDTO;
import fr.av.mbds.myfidelity.dto.ResponseLoginDTO;
import fr.av.mbds.myfidelity.dto.ResponseRegisterDTO;
import fr.av.mbds.myfidelity.util.GlobalProperties;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiService {
    private static ApiService instance;
    private Retrofit retrofit;
    private MyApiService service;
    private Context context;
    private SharedPreferences sharedPreferences;

    public static ApiService getInstance(Context context){
        if(instance == null){
            instance = new ApiService(context);
        }

        return instance;
    }

    public ApiService(Context context){
        this.context = context;
        retrofit = new Retrofit.Builder()
                .baseUrl(GlobalProperties.getConfigValue(context, "url-api"))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(MyApiService.class);
        sharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE);
    }

    public void getToken(String login, String password, Callback callback) {
        Call<ResponseLoginDTO> call = service.login(new RequestLoginDTO(login, password));
        call.enqueue(callback);
    }

    public void getReceiptList(Callback callback){
        String token = "Bearer " + sharedPreferences.getString("token", null);
        Call<List<ReceiptDTO>> call = service.receipts(token);
        call.enqueue(callback);
    }

    public void register(RegisterDTO registerDTO, Callback callback){
        Call<ResponseRegisterDTO> call = service.register(registerDTO);
        call.enqueue(callback);
    }

    public void getBalanceList(Callback callback){
        String token = "Bearer " + sharedPreferences.getString("token", null);
        Call<List<BalanceDTO>> call = service.balances(token);
        call.enqueue(callback);
    }
}
