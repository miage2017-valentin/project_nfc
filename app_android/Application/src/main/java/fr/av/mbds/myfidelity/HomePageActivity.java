package fr.av.mbds.myfidelity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.fragment.ListReceiptFragment;
import fr.av.mbds.myfidelity.fragment.MyCardFragment;
import fr.av.mbds.myfidelity.fragment.ReceiptDetailsFragment;

public class HomePageActivity extends Activity implements ICallable {
    private Context context;
    private AlertDialog errorDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent serviceIntent = new Intent(getApplicationContext(), RefreshService.class);
        startService(serviceIntent);
        setContentView(R.layout.activity_homepage);

        BottomNavigationView navigation = findViewById(R.id.menu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        showFragment(new ListReceiptFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.listReceipt:
                    showFragment(new ListReceiptFragment());
                    return true;
                case R.id.myCard:
                    showFragment(new MyCardFragment());
                    return true;
            }
            return false;
        }

    };


    private void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = this.getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    @Override
    public void showReceiptDetails(ReceiptDTO receipt) {
        ReceiptDetailsFragment receiptDetailsFragment = new ReceiptDetailsFragment();
        receiptDetailsFragment.setReceipt(receipt);
        this.showFragment(receiptDetailsFragment);
    }

    @Override
    public void showListReceipts() {
        showFragment(new ListReceiptFragment());
    }
}
