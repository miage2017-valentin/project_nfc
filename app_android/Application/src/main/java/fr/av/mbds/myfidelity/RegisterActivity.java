package fr.av.mbds.myfidelity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import fr.av.mbds.myfidelity.dto.RegisterDTO;
import fr.av.mbds.myfidelity.dto.ResponseRegisterDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends Activity {
    private Button btnRegister, btnReturnLogin;
    private EditText txtEmail;
    private EditText txtPassword, txtPasswordConfirm;
    private EditText txtName;
    private EditText txtLastName;
    private EditText txtAddress;
    private EditText txtCity;
    private Context context;
    private AlertDialog errorDialog;
    private AlertDialog successDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.context = this;
        this.txtEmail = findViewById(R.id.txtEmail);
        this.txtName = findViewById(R.id.txtName);
        this.txtAddress = findViewById(R.id.txtAddress);
        this.txtCity = findViewById(R.id.txtCity);
        this.txtLastName = findViewById(R.id.txtLastName);
        this.txtPassword = findViewById(R.id.txtPassword);
        this.btnRegister = findViewById(R.id.btnRegisterConfirm);
        this.btnReturnLogin = findViewById(R.id.btnReturnLogin);
        this.txtPasswordConfirm = findViewById(R.id.txtPasswordConfirm);
        this.successDialog = new AlertDialog.Builder(RegisterActivity.this).create();
        successDialog.setTitle("Success");
        successDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        this.errorDialog = new AlertDialog.Builder(RegisterActivity.this).create();
        errorDialog.setTitle("Error");
        errorDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        this.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final RegisterDTO registerDTO = new RegisterDTO();
                    registerDTO.setAddress(txtAddress.getText().toString());
                    registerDTO.setCity(txtCity.getText().toString());
                    registerDTO.setEmail(txtEmail.getText().toString());
                    registerDTO.setName(txtName.getText().toString());
                    registerDTO.setLastName(txtLastName.getText().toString());
                    registerDTO.setPassword(txtPassword.getText().toString());
                    registerDTO.setPasswordConfirm(txtPasswordConfirm.getText().toString());

                    if(!registerDTO.checkValid()){
                        throw new Exception("All input must be fill");
                    }

                    ApiService.getInstance(context).register(registerDTO, new Callback<ResponseRegisterDTO>() {
                        @Override
                        public void onResponse(Call<ResponseRegisterDTO> call, Response<ResponseRegisterDTO> response) {
                            if(response.errorBody() == null && response.body() != null){
                                successDialog.setMessage(response.body().getMessage());
                                successDialog.show();
                                setResult(Activity.RESULT_OK,
                                        new Intent()
                                                .putExtra("email", registerDTO.getEmail())
                                                .putExtra("password", registerDTO.getPassword()));
                            }else{
                                Gson gson = new Gson();
                                TypeAdapter<ResponseRegisterDTO> adapter = gson.getAdapter(ResponseRegisterDTO.class);
                                try {
                                    throw new Exception(adapter.fromJson(response.errorBody().string()).getMessage());
                                } catch (Exception e) {
                                    errorDialog.setMessage(e.getMessage());
                                    errorDialog.show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                            errorDialog.setMessage(t.getMessage());
                            errorDialog.show();
                        }
                    });
                } catch (Exception e) {
                    errorDialog.setMessage(e.getMessage());
                    errorDialog.show();
                }
            }
        });

        this.btnReturnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
