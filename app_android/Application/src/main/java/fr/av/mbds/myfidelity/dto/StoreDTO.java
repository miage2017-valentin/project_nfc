package fr.av.mbds.myfidelity.dto;

public class StoreDTO {
    private String name;
    private String image;
    private String address;

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getAddress() {
        return address;
    }
}
