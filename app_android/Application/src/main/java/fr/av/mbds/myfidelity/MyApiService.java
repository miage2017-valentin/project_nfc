package fr.av.mbds.myfidelity;

import java.util.List;

import fr.av.mbds.myfidelity.dto.BalanceDTO;
import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.dto.RegisterDTO;
import fr.av.mbds.myfidelity.dto.RequestLoginDTO;
import fr.av.mbds.myfidelity.dto.ResponseLoginDTO;
import fr.av.mbds.myfidelity.dto.ResponseRegisterDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MyApiService {
    @POST("login")
    Call<ResponseLoginDTO> login(@Body RequestLoginDTO request);

    @GET("receipts")
    Call<List<ReceiptDTO>> receipts(@Header("Authorization") String token);

    @GET("balances")
    Call<List<BalanceDTO>> balances(@Header("Authorization") String token);

    @POST("register")
    Call<ResponseRegisterDTO> register(@Body RegisterDTO request);
}