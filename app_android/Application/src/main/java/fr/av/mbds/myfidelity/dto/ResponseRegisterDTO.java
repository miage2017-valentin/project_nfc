package fr.av.mbds.myfidelity.dto;

public class ResponseRegisterDTO {
    private String message;
    private boolean auth;
    private String token;

    public String getMessage() {
        return message;
    }

    public boolean isAuth() {
        return auth;
    }

    public String getToken() {
        return token;
    }
}
