package fr.av.mbds.myfidelity.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import fr.av.mbds.myfidelity.ICallable;
import fr.av.mbds.myfidelity.R;
import fr.av.mbds.myfidelity.adapter.PurchaseAdapter;
import fr.av.mbds.myfidelity.adapter.ReceiptAdapter;
import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.util.GlobalProperties;
import fr.av.mbds.myfidelity.util.ItemClickSupport;

public class ReceiptDetailsFragment extends Fragment {
    private ReceiptDTO receipt;
    private Context context;
    private ICallable mCallback;
    private RecyclerView listViewPurchases;
    private TextView txtLabelStoreName, txtLabelTotalAmount, txtLabelPaymentWith;
    private FloatingActionButton btnReturn;
    private ImageView imageStore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_receipt_details, container, false);
        listViewPurchases = view.findViewById(R.id.listPurchase);
        txtLabelStoreName = view.findViewById(R.id.labelStoreName);
        txtLabelPaymentWith = view.findViewById(R.id.labelPaymentWith);
        txtLabelTotalAmount = view.findViewById(R.id.labelTotalAmount);
        btnReturn = view.findViewById(R.id.btnReturn);
        imageStore = view.findViewById(R.id.imageStore);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        listViewPurchases.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listViewPurchases.getContext(),
                mLayoutManager.getOrientation());
        listViewPurchases.addItemDecoration(dividerItemDecoration);

        initView();
        refreshListPurchases();
        return view;
    }

    private void initView(){
        this.txtLabelTotalAmount.setText(this.receipt.getTotalAmount().toString() + "€");
        this.txtLabelPaymentWith.setText(this.receipt.getMeansOfPayment());
        this.txtLabelStoreName.setText(this.receipt.getStore().getName());
        Picasso.get().load(GlobalProperties.getConfigValue(context, "url-api") + receipt.getStore().getImage())
                .into(imageStore);
        this.btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.showListReceipts();
            }
        });
    }

    private void refreshListPurchases() {
        final PurchaseAdapter adapter = new PurchaseAdapter(receipt.getPurchases());
        listViewPurchases.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof ICallable) {
            mCallback = (ICallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable");
        }
    }

    public void setReceipt(ReceiptDTO receipt){
        this.receipt = receipt;
    }
}
