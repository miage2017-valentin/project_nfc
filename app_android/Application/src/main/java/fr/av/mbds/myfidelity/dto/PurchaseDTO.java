package fr.av.mbds.myfidelity.dto;

public class PurchaseDTO {
    private int quantity;
    private ProductDTO product;

    public int getQuantity() {
        return quantity;
    }

    public ProductDTO getProduct() {
        return product;
    }
}
