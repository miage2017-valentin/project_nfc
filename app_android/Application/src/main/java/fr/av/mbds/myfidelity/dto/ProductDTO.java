package fr.av.mbds.myfidelity.dto;

import java.math.BigDecimal;

public class ProductDTO {
    private String name;
    private BigDecimal unitaryPrice;
    private Long loyaltyPercentage;

    public String getName() {
        return name;
    }

    public BigDecimal getUnitaryPrice() {
        return unitaryPrice;
    }

    public Long getLoyaltyPercentage() {
        return loyaltyPercentage;
    }
}
