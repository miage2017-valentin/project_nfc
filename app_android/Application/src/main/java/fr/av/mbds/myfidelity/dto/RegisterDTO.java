package fr.av.mbds.myfidelity.dto;

public class RegisterDTO {
    private String email;
    private String name;
    private String lastName;
    private String address;
    private String city;
    private String password;
    private String passwordConfirm;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public boolean checkValid(){
        return !this.address.isEmpty()
                && !this.city.isEmpty()
                && !this.lastName.isEmpty()
                && !this.name.isEmpty()
                && !this.email.isEmpty()
                && !this.password.isEmpty();
    }
}
