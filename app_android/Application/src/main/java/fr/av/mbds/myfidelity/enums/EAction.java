package fr.av.mbds.myfidelity.enums;

public enum EAction {
    PAYMENT_WITH_LOYALTY_CARD, JUST_RECEIPT
}
