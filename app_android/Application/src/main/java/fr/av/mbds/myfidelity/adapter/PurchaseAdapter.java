package fr.av.mbds.myfidelity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.RoundingMode;
import java.util.List;

import fr.av.mbds.myfidelity.R;
import fr.av.mbds.myfidelity.dto.PurchaseDTO;

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.MyViewHolder> {
    private List<PurchaseDTO> dataSet;

    public PurchaseAdapter(List<PurchaseDTO> data) {
        this.dataSet = data;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PurchaseDTO receipt = dataSet.get(position);
        holder.labelNameOfProduct.setText(receipt.getProduct().getName());
        holder.labelQuantityOfProduct.setText(Integer.toString(receipt.getQuantity()));
        holder.labelUnitaryPriceOfProduct.setText(receipt.getProduct().getUnitaryPrice().setScale(2, RoundingMode.HALF_UP).toString() + "€");
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_purchase, parent, false);
        return new MyViewHolder(v);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView labelNameOfProduct, labelUnitaryPriceOfProduct, labelQuantityOfProduct;

        public MyViewHolder(View v) {
            super(v);
            this.labelNameOfProduct = v.findViewById(R.id.labelNameOfProduct);
            this.labelUnitaryPriceOfProduct = v.findViewById(R.id.labelUnitaryPriceOfProduct);
            this.labelQuantityOfProduct = v.findViewById(R.id.labelQuantityOfProduct);
        }
    }

    public PurchaseDTO getPurchase(int position) {
        return this.dataSet.get(position);
    }
}
