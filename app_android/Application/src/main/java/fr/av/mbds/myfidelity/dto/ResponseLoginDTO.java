package fr.av.mbds.myfidelity.dto;

public class ResponseLoginDTO {

    private String token;
    private boolean auth;
    private String message;

    public ResponseLoginDTO(String token, boolean auth, String message) {
        this.token = token;
        this.auth = auth;
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public String getMessage(){
        return message;
    }
}
