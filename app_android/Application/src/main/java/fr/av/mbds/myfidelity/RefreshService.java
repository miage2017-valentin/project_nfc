package fr.av.mbds.myfidelity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.concurrent.atomic.AtomicInteger;

public class RefreshService extends Service {
    private Handler mHandler;
    // default interval for syncing data
    private static long nbSecond = 1;

    // task to be run here
    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent("REFRESH_LIST");
            sendBroadcast(intent);
            mHandler.postDelayed(runnableService, nbSecond * 1000);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create the Handler object
        mHandler = new Handler();
        // Execute a runnable task as soon as possible
        mHandler.post(runnableService);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

