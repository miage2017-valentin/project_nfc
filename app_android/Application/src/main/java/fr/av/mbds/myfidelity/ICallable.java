package fr.av.mbds.myfidelity;

import fr.av.mbds.myfidelity.dto.ReceiptDTO;

public interface ICallable {
    void showReceiptDetails(ReceiptDTO receipt);
    void showListReceipts();
}
