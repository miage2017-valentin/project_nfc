package fr.av.mbds.myfidelity.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.List;

import fr.av.mbds.myfidelity.ApiService;
import fr.av.mbds.myfidelity.R;
import fr.av.mbds.myfidelity.adapter.BalanceAdapter;
import fr.av.mbds.myfidelity.adapter.ReceiptAdapter;
import fr.av.mbds.myfidelity.dto.BalanceDTO;
import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.util.ItemClickSupport;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCardFragment extends Fragment {
    private CheckBox chkPaymentWithLoyaltyPoint;
    private RecyclerView listViewBalances;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_my_card, container, false);
        this.listViewBalances = view.findViewById(R.id.listViewBalances);
        final SharedPreferences mPrefs = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        this.chkPaymentWithLoyaltyPoint = view.findViewById(R.id.chkPaymentWithLoyaltyPoint);
        chkPaymentWithLoyaltyPoint.setChecked(mPrefs.getBoolean("paymentWithLoyaltyPoint", false));
        chkPaymentWithLoyaltyPoint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences.Editor ed = mPrefs.edit();
                ed.putBoolean("paymentWithLoyaltyPoint", b).commit();
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        listViewBalances.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listViewBalances.getContext(),
                mLayoutManager.getOrientation());
        listViewBalances.addItemDecoration(dividerItemDecoration);

        refreshListBalance();
        return view;
    }

    private void refreshListBalance() {
        ApiService.getInstance(context).getBalanceList(new Callback<List<BalanceDTO>>() {
            @Override
            public void onResponse(Call<List<BalanceDTO>> call, Response<List<BalanceDTO>> response) {
                List<BalanceDTO> listBalances = response.body();

                final BalanceAdapter adapter = new BalanceAdapter(listBalances, context);
                listViewBalances.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                System.err.println(t.getMessage());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        /*if (context instanceof ICallable) {
            mCallback = (ICallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable");
        }*/
    }
}
