package fr.av.mbds.myfidelity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import fr.av.mbds.myfidelity.R;
import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.util.GlobalProperties;

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.MyViewHolder> {
    private List<ReceiptDTO> dataSet;
    private Context context;

    public ReceiptAdapter(List<ReceiptDTO> data, Context context) {
        this.dataSet = data;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        ReceiptDTO receipt = dataSet.get(position);
        holder.txtStore.setText(receipt.getStore().getName());
        holder.txtAmount.setText(receipt.getTotalAmount().toString() + "€");
        holder.txtCreatedAt.setText(dateFormat.format(receipt.getCreatedAt()));
        Picasso.get().load(GlobalProperties.getConfigValue(context, "url-api") + receipt.getStore().getImage())
                .into(holder.imageStore);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_receipt, parent, false);
        return new MyViewHolder(v);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtCreatedAt;
        public TextView txtAmount;
        public TextView txtStore;
        public ImageView imageStore;

        public MyViewHolder(View v) {
            super(v);
            this.txtStore = v.findViewById(R.id.labelStore);
            this.txtCreatedAt = v.findViewById(R.id.labelDate);
            this.txtAmount = v.findViewById(R.id.labelTotalAmountOfReceipt);
            this.imageStore = v.findViewById(R.id.imageStore);
        }
    }

    public ReceiptDTO getReceipt(int position) {
        return this.dataSet.get(position);
    }
}
