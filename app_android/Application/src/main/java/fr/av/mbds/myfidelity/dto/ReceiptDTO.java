package fr.av.mbds.myfidelity.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ReceiptDTO {

    private StoreDTO store;
    private BigDecimal totalAmount;
    private Date createdAt;
    private String meansOfPayment;
    private List<PurchaseDTO> purchases;

    public StoreDTO getStore() {
        return store;
    }

    public BigDecimal getTotalAmount(){
        return totalAmount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public List<PurchaseDTO> getPurchases() {
        return purchases;
    }

    public String getMeansOfPayment() {
        return meansOfPayment;
    }
}
