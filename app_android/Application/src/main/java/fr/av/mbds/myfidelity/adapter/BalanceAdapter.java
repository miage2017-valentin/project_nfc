package fr.av.mbds.myfidelity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.math.RoundingMode;
import java.util.List;

import fr.av.mbds.myfidelity.R;
import fr.av.mbds.myfidelity.dto.BalanceDTO;
import fr.av.mbds.myfidelity.dto.ReceiptDTO;
import fr.av.mbds.myfidelity.util.GlobalProperties;

public class BalanceAdapter extends RecyclerView.Adapter<BalanceAdapter.MyViewHolder>  {

    private List<BalanceDTO> dataSet;
    private Context context;

    public BalanceAdapter(List<BalanceDTO> data, Context context) {
        this.dataSet = data;
        this.context = context;
    }

    @Override
    public BalanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_balance, parent, false);
        return new BalanceAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BalanceDTO balance = dataSet.get(position);
        holder.txtStoreName.setText(balance.getStore().getName());
        holder.txtBalance.setText(balance.getAmount().setScale(2, RoundingMode.HALF_UP).toString() + "€");
        Picasso.get().load(GlobalProperties.getConfigValue(context, "url-api") + balance.getStore().getImage())
                .into(holder.imageStore);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtStoreName;
        public TextView txtBalance;
        public ImageView imageStore;

        public MyViewHolder(View v) {
            super(v);
            this.txtStoreName = v.findViewById(R.id.labelStoreName);
            this.txtBalance = v.findViewById(R.id.labelActualBalance);
            this.imageStore = v.findViewById(R.id.imageStore);
        }
    }

    public BalanceDTO getBalance(int position) {
        return this.dataSet.get(position);
    }
}
