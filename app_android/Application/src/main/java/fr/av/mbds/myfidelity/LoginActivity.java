package fr.av.mbds.myfidelity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.io.IOException;

import fr.av.mbds.myfidelity.dto.ResponseLoginDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {
    private Button btnLogin;
    private Button btnRegister;
    private EditText txtEmail;
    private EditText txtPassword;
    private Context context;
    private AlertDialog errorDialog;
    private static final int REQUEST_NEW_ACCOUNT = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.context = this;
        this.txtEmail = findViewById(R.id.txtEmail);
        this.txtPassword = findViewById(R.id.txtPassword);
        this.btnLogin = findViewById(R.id.btnLogin);
        this.btnRegister = findViewById(R.id.btnRegister);
        this.errorDialog = new AlertDialog.Builder(LoginActivity.this).create();
        errorDialog.setTitle("Error");
        errorDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "",
                        "Loading. Please wait...", true);
                try {
                    dialog.show();
                    ApiService.getInstance(context).getToken(txtEmail.getText().toString(), txtPassword.getText().toString(),
                            new Callback<ResponseLoginDTO>() {
                                @Override
                                public void onResponse(Call<ResponseLoginDTO> call, Response<ResponseLoginDTO> response) {
                                    dialog.hide();
                                    if (response.errorBody() != null){
                                        Gson gson = new Gson();
                                        TypeAdapter<ResponseLoginDTO> adapter = gson.getAdapter(ResponseLoginDTO.class);
                                        try {
                                            throw new Exception(adapter.fromJson(response.errorBody().string()).getMessage());
                                        } catch (Exception e) {
                                            errorDialog.setMessage(e.getMessage());
                                            errorDialog.show();
                                        }
                                    }else{
                                        SharedPreferences mPrefs = getSharedPreferences("data", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = mPrefs.edit();
                                        ed.putString("token", response.body().getToken()).commit();
                                        showHomePage(view);
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseLoginDTO> call, Throwable t) {
                                    dialog.hide();
                                    errorDialog.setMessage(t.getMessage());
                                    errorDialog.show();
                                }
                            });
                } catch (Exception e) {
                    errorDialog.setMessage(e.getMessage());
                    errorDialog.show();
                }
            }
        });

        this.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRegisterForm(view);
            }
        });
    }

    public void showHomePage(View view) {
        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
        finish();
    }

    public void showRegisterForm(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, REQUEST_NEW_ACCOUNT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_NEW_ACCOUNT && resultCode == Activity.RESULT_OK) {
            this.txtEmail.setText(data.getStringExtra("email"));
            this.txtPassword.setText(data.getStringExtra("password"));
        }
    }
}
