package fr.av.mbds.myfidelity.dto;

import java.math.BigDecimal;

public class BalanceDTO {
    private StoreDTO store;
    private BigDecimal amount;

    public StoreDTO getStore() {
        return store;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
