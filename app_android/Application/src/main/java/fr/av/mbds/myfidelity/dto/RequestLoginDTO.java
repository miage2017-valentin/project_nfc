package fr.av.mbds.myfidelity.dto;

public class RequestLoginDTO {
    String email;
    String password;

    public RequestLoginDTO(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
